
#### Downsampling analysis to characterize discovery cohort misclassification vs. no of CpG features

def list_subsamples(wildcards):
    checkpoint_output = checkpoints.downsampleCpGs.get(sample=wildcards.sample).output["fofn"]
    return expand("classification/{i}-votes-ScikitlearnRF-{trainingset}.tsv", i = open(checkpoint_output, "r").read().splitlines(), trainingset = wildcards.trainingset)

rule merge_subsamples_per_sample:
    input: list_subsamples
    output: "perSample/summaryDownsampling_{sample}_with_{trainingset}.tsv"
    shell: "head -1 {input[0]} > {output} ; tail -n +2 -q {input} >> {output}"

rule merge_subsamples:
    input: expand("perSample/summaryDownsampling_{sample}_with_{{trainingset}}.tsv", sample = config["samples"])
    output: "summaryDownsampling_{trainingset}.tsv"
    shell: "head -1 {input[0]} > {output} ; tail -n +2 -q {input} >> {output}"

checkpoint downsampleCpGs:
    input: "methylation/{sample}.RData"
    params:
        seeds = config["seed"],
        cpgs = config["CpGs"]
    output: fofn = "methylation/{sample}.fofn"
    conda: nanoDx("envs/readCpGs.yaml")
    priority: 100
    script: "scripts/downsampleCpGs.R"

rule downsamplePlot:
    input:
      summary="summaryDownsampling_Capper_et_al.tsv",
      ground_truth="static/Ground_Truth.txt"
    output:
        Figure1 = "plots/Figure1.pdf",
        roc = "summary_roc_measures.tsv"
    conda: "envs/basicR.yaml"
    script: "scripts/downsamplePlot.R"
