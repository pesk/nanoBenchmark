rule extract_readIDs_with_timepoints_and_sequence_length:
    input: nanoDx("fastq/{sample}.fq")
    output:
        timepoints = "tables/timepoints_{sample}.tsv",
        seqlength = "tables/seqlength_{sample}.tsv"
    conda: "envs/bioawk.yaml"
    shell:
        """
        awk 'sub(/^@/,"") && sub(/ .*start_time=/,"\t")' {input} > {output.timepoints}
        bioawk -cfastx '{{print $name, length($seq)}}' {input} > {output.seqlength}
        """
rule return_readIDs_per_timeframe:
    input: "tables/timepoints_{sample}.tsv"
    output: "timeframe/{sample}_{timeframe}.tsv"
    conda: nanoDx("envs/basicR.yaml")
    script: "scripts/read_ID_timeframe_extraction.R"

rule extract_only_readIDs_per_timeframe:
    input: "timeframe/{sample}_{timeframe}.tsv"
    output: "readIDs/{sample}_{timeframe}.tsv"
    shell: """awk -F "\t" '{{print $2}}' {input} | awk '{{print substr($0,2,length()-2);}}' > {output}"""

rule extract_timeframes_from_methylation_call:
    input:
        calls = nanoDx("methylation/{sample}.calls"),
        readIDs = "readIDs/{sample}_{timeframe}.tsv"
    output: temporary("methylation/{sample}_t{timeframe}.calls")
    shell:
        "head -1 {input.calls} > {output} ; "
        "grep -w -F -f {input.readIDs} {input.calls} >> {output}"

rule pileup_CpG:
    input:
        "methylation/{sample}_t{timeframe}.calls"
    output:
        "methylation/{sample}_t{timeframe}.pileup.txt"
    shell:
        "python ../nanoDx/scripts/calculate_methylation_frequency.py -c 2.5 -s {input} > {output}"

rule readCpGs:
    input:
        pileup="methylation/{sample}_t{timeframe}.pileup.txt",
        mapping=config["450K_hg19"]
    output:
        "methylation/{sample}-t{timeframe}.RData"
    wildcard_constraints:
        sample = '|'.join(config["samples"])
    conda: nanoDx("envs/readCpGs.yaml")
    script:
        "scripts/readCpGs.R"

rule merge_sequencing_time_tsv:
    input: expand("classification/{sample}-t{timeframe}-votes-ScikitlearnRF-{{trainingset}}.tsv", sample=config["samples-1000"], timeframe=config["timeframe"])
    output: "summary_sequencing_time_{trainingset}.tsv"
    shell: "awk 'NR == 1 || FNR > 1'  {input} > {output}"

#### Figure 3 - Benchmarking of turnaround timepoints

rule summaryOverlap:
     input: expand("methylation/{sample}-t{timeframe}.RData", sample = config["samples"], timeframe = config["timeframe"])
     output: "summaryOverlap.tsv"
     conda: "envs/basicR.yaml"
     script: "scripts/count_CpG_overlap.R"

rule Figure3:
    input:
        redcap = "static/NanoDx_DATA_2020-09-24_1947.csv",
        data = "summary_sequencing_time_Capper_MCF.tsv",
        ground_truth = "static/Ground_Truth.txt",
        classification = "summaryClassification-ScikitlearnRF-Capper_MCF.tsv",
        classification1 = "summaryClassification-ScikitlearnRF-Capper_MCF.tsv",
        cpg = "summaryOverlap.tsv"
    params: config["PROSPECTIVE"]
    output:
        fig3_1 = "plots/Figure3_1.pdf",
        fig3_2 = "plots/Figure3_2.pdf",
        fig3a = "plots/Fig3A.pdf",
        fig3b = "plots/Fig3B.pdf",
        sum_seq_time = "summary_sequencing_time_filtered.tsv",
        rc = "redcap_extracted_data.tsv",
        pre_handson = "pre_handson_extracted_data.tsv"
    conda: "envs/basicR.yaml"
    script: "scripts/Figure3.R"
