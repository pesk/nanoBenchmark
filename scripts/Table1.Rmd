```{r table1, echo=FALSE, message=FALSE, warning=FALSE}
library(table1)

data$reference.diagnosis <- as.character(data$reference.diagnosis)
label(data$reference.diagnosis.simple) <- "WHO 2016 integrated diagnosis"

table1(~ reference.diagnosis.simple | series, data=data)
```
* Of note, these entities are not yet recognized as distinct entities in the 2016 WHO classification.
