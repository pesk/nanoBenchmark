library(dplyr)
library(data.table)

mc <- fread(snakemake@input[["mc"]], header = T)
mcf <- fread(snakemake@input[["mcf"]], header = T)

data <- mc %>% left_join(mcf, by="sampleID", suffix=c("",".y")) %>% select(-ends_with(".y"))
data$V1 <- NULL

write.csv(data, file = snakemake@output[[1]], row.names = FALSE)