# -*- coding: utf-8 -*-
"""
Editor: DONGSHENG

This is a temporary script file.
"""

#####  to repalce the R script
import numpy as np  
import pandas as pd
import h5py
import pyreadr
from sklearn.ensemble import RandomForestClassifier
import logging
import collections
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import brewer2mpl 
import warnings
from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.calibration import CalibratedClassifierCV

warnings.filterwarnings("ignore")
#print('input',snakemake.input)
# generate methylation training set from .h5 file
print(' Reading fh5 trainingset_meth.')

fh5 =snakemake.input["trainingset_meth"]

#fh5 = '/fast/projects/nanodx/work/trainingSets/Capper_et_al.h5'
print(fh5)
#fh5='/fast/projects/nanodx/work/trainingSets/pancan_devel_v5.h5' 
fh5=h5py.File(fh5)
label=list(fh5['Dx'][:])
label=[str(x).replace('b','').replace('\'','') for x in label]
probeIDs=list(fh5['probeIDs'][:])
probeIDs=[str(x).replace('b','').replace('\'','') for x in probeIDs]
train_data=fh5['betaValues']



print(' Reading METH_RDATA.')

# load input METH_RDATA
METH_RDATA =snakemake.input["meth"]
print(METH_RDATA)
#
case=pyreadr.read_r(METH_RDATA)
case=pd.DataFrame(data=list(case.items())[0][1]).T

def matchfeatures(validdata,probeIDs,data):
    # overlaping CpG features with ref data
    probes=[i for i in range(len(probeIDs)) if probeIDs[i] in validdata.columns]
    probesid = [probeIDs[i] for i in probes]
    
    #message(paste(length(probes)," overlapping CpG sites between sample and reference set. Reading training set now...",sep=""))
    logging.info(len(probesid),' overlapping CpG sites between sample and reference set. Reading training set now...') 
    print(len(probesid),' overlapping CpG sites between sample and reference set. Reading training set now...') 
    
    test_data=validdata[probesid]
    
    train_data=data[probes,:]
    
    train_data = pd.DataFrame(train_data.T,columns=probesid)
    return train_data,test_data 


#X_train,X_test=matchfeatures(case['case'],probeIDs,train_data)
X_train,X_test=matchfeatures(case,probeIDs,train_data)
fh5.close()


X_train[X_train<=0.6]=0
X_train[X_train>0.6]=1   



#message(paste(length(probes)," overlapping CpG sites read from training set.",sep=""))
logging.info(X_train.shape[1],' overlapping CpG sites read from training set.') 
print(X_train.shape[1],' overlapping CpG sites read from training set.')


#### feature selection (top half by SD)
#
max_CpG = snakemake.params["max_CpG"]
#max_CpG = 50000
sds=X_train.std()
#sds.rank(ascending=False)


maxSDs=sds.sort_values(ascending=False).index[0:min([max_CpG,X_train.shape[1]])]
X_train=X_train[maxSDs.to_list()]
X_test=X_test[maxSDs.to_list()]

#message(paste(length(maxSDs)," features selected for training random forest.",sep=""))
logging.info(len(maxSDs)," features selected for training random forest.") 
print(len(maxSDs)," features selected for training random forest.")



### classify
#rf =RandomForestClassifier(criterion='entropy',  min_samples_split=4, min_samples_leaf=1,
#                       n_estimators=500,random_state=42,n_jobs= 12,oob_score=True)
rf = RandomForestClassifier( random_state=42 , n_estimators=5000,n_jobs= 12,oob_score=True)
rf.fit(X_train, label)

pre_rf=rf.predict(X_test)
#pre_class=rf.predict(X_test)[0]
x_score=max(rf.predict_proba(X_test)[0])
pre_class=pre_rf[0]
### predict case and get the votes from each trees

def pre_singletree(est,X_input,classes_):
    res=int(est.predict(X_input))
    return classes_[res],res

predictions_trees=list(map(lambda i:pre_singletree(i, X_test,rf.classes_), rf.estimators_))



tmp = [i[0] for i in predictions_trees]
rownames=[i[0] for i in collections.Counter(tmp).items()]
Freq=[i[1] for i in collections.Counter(tmp).items()]
Var1= [i[1] for i in predictions_trees]

votes = pd.DataFrame(list(zip(Var1, Freq)),
               columns =['Var1', 'Freq'], index= rownames)
votes['Freq']=votes['Freq']/sum(votes['Freq']) *100
votes=votes.sort_values(by=['Freq'])

## vote info will be deleted later
votes['info'] = votes.index
#### CV calibration
def get_proba_CalibratedClassifierCV(X_train,y_train):
    cv = RepeatedStratifiedKFold(n_splits=5,n_repeats=1,random_state=1)
    #clf = RandomForestClassifier(criterion='entropy',  min_samples_split=4, min_samples_leaf=1,
    #                   n_estimators=500,random_state=42,n_jobs= 12,oob_score=True)
    clf = RandomForestClassifier( random_state=42 , n_estimators=5000,n_jobs= 12,oob_score=True)
    clf_sigmoid = CalibratedClassifierCV(clf, cv=cv, method='sigmoid',n_jobs =-1,ensemble=False)
    clf_sigmoid.fit(X_train, y_train)
    res_proba=clf_sigmoid.predict_proba(X_test)
    # proba=res_proba[0][clf_sigmoid.classes_.tolist().index(pre_class)]
    proba = pd.DataFrame(data={'cal_Freq': res_proba[0]} , index=clf_sigmoid.classes_ )
    return proba

proba=get_proba_CalibratedClassifierCV(X_train,label)

df=proba.join(votes).fillna(0)


# write output calibration
report_content=['Number of features: '+str(rf.n_features_),
                "Predicted Class: "+pre_rf[0],
                "Initial Score: "+ str(x_score),
                "Calibrated Score: "+ str( proba.loc[pre_class].values[0])
               ]
report="\n".join(report_content)

#with open('testdont_readme.txt', 'w') as f:
with open(snakemake.output["calibration"], 'w') as f:
    f.write(report)

####
# output votes
#save(rf, votes, file=snakemake@output[["votes"]])
#pyreadr.write_rdata("test.RData", votes, df_name="votes")

pyreadr.write_rdata(snakemake.output["votes"], df, df_name="votes")
oob_numfeature=pd.DataFrame([rf.oob_score_,rf.n_features_,pre_class])
#pyreadr.write_rdata("model_info.RData", votes, df_name="votes")

pyreadr.write_rdata(snakemake.output["model_info"], oob_numfeature, df_name="oob_numfeature")

####Now required now
# X_train.insert(loc=0, column='Dx', value=label)
# X_train.loc[len(X_train)]=["unknown"]+list(X_test.iloc[0,:])



# #pyreadr.write_rdata("test.RData", X_train, df_name="matrix")
# pyreadr.write_rdata(snakemake.output["matrix"], X_train, df_name="matrix")

#snakemake@output[["pdf"]]

sample_name=snakemake.wildcards["sample"]
#sample_name='test'
#with PdfPages('test_page_pdf.pdf') as pdf:
with PdfPages(snakemake.output["pdf"]) as pdf:
    plt.subplot(2, 1, 1)
    plt.title(sample_name)
    
    plt.pie(votes['Freq'],labels=list(votes.index),startangle=90,colors=brewer2mpl.get_map('Set1', 'qualitative', 9).mpl_colors)
    plt.subplot(2, 1, 2)
    plt.pie(df['cal_Freq'],labels=list(df.index),startangle=90,colors=brewer2mpl.get_map('Set1', 'qualitative', 9).mpl_colors)
    pdf.savefig()
    plt.close()
