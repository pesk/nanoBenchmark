library(data.table)
library(dplyr)

f <- snakemake@input[[1]]
data <- fread(f, header = T)
data$runID <- basename(sub("(.+)_demux_stat.tsv","\\1",f ))

counts <- data %>% count(runID, barcode)

write.csv(counts, snakemake@output[[1]])


