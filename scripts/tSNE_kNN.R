library(data.table)
library(RColorBrewer)
library(Rtsne)
library(class)

### load methylation calls

METH_RDATA <- snakemake@input[["meth"]]

load(METH_RDATA)
case <- data.frame(t(sapply(case, function(x) as.numeric(as.character(x)))))

### generate methylation training set

library(rhdf5)
fh5 = snakemake@input[["trainingset_meth"]]

# dump HDF5 training set content
h5ls(fh5)

Dx <- as.factor(h5read(fh5,"Dx"))
sampleIDs <- h5read(fh5,"sampleIDs")
trainingProbes <- h5read(fh5,"probeIDs")

probes <- intersect(colnames(case), trainingProbes)
idxs <- match(probes, trainingProbes)

message(paste(length(probes)," overlapping CpG sites between sample and reference set. Reading training set now...",sep=""))

# Binarize using threshold > 0.6
ts <- data.frame(Dx, (as.matrix(h5read(fh5, "betaValues")) > 0.6)[,idxs] * 1)
colnames(ts) <- c("Dx", trainingProbes[idxs])

#### remove cases without Dx
ts <- subset(ts, Dx != "NA")
ts$Dx <- droplevels(ts$Dx)

message(paste(length(probes)," overlapping CpG sites read from training set.",sep=""))

#### feature selection (top half by SD)

max_CpG <- snakemake@params[["max_CpG"]]
library(matrixStats)
sds <- colSds(as.matrix(ts[,-1]), na.rm=F)
maxSDs <- head(order(sds,decreasing=T),n = min(ncol(ts)-1, max_CpG))
ts <- ts[,c(1,maxSDs + 1)]

message(paste(length(maxSDs)," features selected for training random forest.",sep=""))

#### identify overlapping features' names

cols <- intersect(colnames(ts),colnames(case))


#### perform tSNE

m <- rbind(ts[,c("Dx",cols)],
           data.frame(Dx="unknown",case[,cols]))

message("Running tSNE...")

tsne <- Rtsne(as.matrix(m[,-1]), partial_pca = T, 
initial_dims = 94, perplexity = 30, theta = 0, max_iter = 2500, 
check_duplicates = F, verbose = T)

### classify by kNN

ts$Dx <- as.factor(ts$Dx)

vote <- knn(tsne$Y[1:nrow(ts),], tsne$Y[nrow(tsne$Y),], cl = ts$Dx, k = 5, l = 0, prob = FALSE, use.all = TRUE)

message(vote)

save.image(file="debug.RData")

### save votes

votes <- data.frame(Freq = rep(0, length(levels(ts$Dx))))
rownames(votes) <- levels(ts$Dx)
votes[as.character(vote),"Freq"] <- 100

# dummy object for compatibility with RF output
rf <- list(prediction.error = 1, num.independent.variables = length(cols))

save(votes, rf, file=snakemake@output[["votes"]])

### plot to PDF

pdf(snakemake@output[["pdf"]])

plot(tsne$Y)

dev.off()

