library(data.table)
library(ggplot2)
library(purrr)
library(dplyr)

tp <- fread(snakemake@input[["tp"]])
gt <- fread(snakemake@input[["gt"]])

tp$sampleID <- sub("(.*)-t(\\d+)","\\1",tp$sample)
tp$timepoint <- as.integer(sub("(.*)-t(\\d+)","\\2",tp$sample))

pre_m <- merge(tp,gt,by.x="sampleID",by.y="runID")

pre_m$isCorrect <- unlist(with(pre_m, map2(compatible.methylation.class.family, majority.vote, grepl)))

remove <- pre_m %>% group_by(sampleID) %>% 
  summarise(independent.variables = max(independent.variables)) %>% 
  filter(independent.variables < 1000)

m <- pre_m[!(pre_m$sampleID %in% remove$sampleID),]

#### plot score over time

ggplot(m, aes(x = timepoint, y=majority.vote.count, group=sampleID)) +
  geom_line() + 
  geom_hline(yintercept = 0.15, linetype="dashed") +
  geom_point(aes(color=isCorrect
                 #,alpha=ifelse(independent.variables > 1000, 1,0.5)
                 )) +
  facet_wrap(~ sampleID, ncol=8) +
  scale_color_brewer(palette = "Set1", name = "Correct Call") +
  theme_classic() +
  ylab("pseudo-probability score") +
  xlab("timepoints in hours")


#### Suppl Fig 4: plot score over time

Supp_Fig4 <- ggplot(m 
                    #%>% filter(independent.variables > 1000)
                    , aes(x = timepoint, y=majority.vote.count, group=sampleID)) +
  geom_line() + 
  geom_hline(yintercept = 0.15, linetype="dashed") +
  geom_point(aes(color=isCorrect)) +
  scale_color_brewer(palette = "Set1", name = "Correct Call") +
  facet_wrap(~ sampleID, ncol=8) +
  xlab("Sequencing time (min)") +
  scale_x_continuous(labels=c(30,60,120,240,360)) +
  ylab("Recalibrated score") + 
  theme_classic()

ggsave(snakemake@output[[1]], Supp_Fig4, width=12,height=8)


### no. CpG after 6hrs

cpg_6hrs <- m %>% 
  group_by(sampleID) %>% 
  summarize(max(independent.variables))

### time to 1000 CpG sites?

t1000 <- m %>% 
          group_by(sampleID) %>% 
          filter(independent.variables > 1000) %>% 
          summarize(min(timepoint))

### time first correct call with score > 0.7

tFirstCorrect <- m %>% 
  group_by(sampleID) %>% 
  filter(isCorrect & majority.vote.count > 70) %>% 
  summarize(min(timepoint))
