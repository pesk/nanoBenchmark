### legacy RF

rule randomForest_PanCancer:
    input:
        meth="methylation/{sample}.RData",
        trainingset_meth=config["trainingset_dir"] + "/{trainingset}.h5"
    params:
        max_CpG = 50000
    output:
        pdf="plots/{sample}-RF-{trainingset}.pdf",
        matrix=temp("classification/{sample}-matrix-{trainingset}.RData"),
        votes="classification/{sample}-votes-RF-{trainingset}.RData"
    benchmark: "benchmarks/{sample}.RF.{trainingset}.benchmark.txt"
    conda: "envs/tsneRF.yaml"
    threads: 12
    script: nanoDx("scripts/classifyByMethylation.R")

rule UMAP_kNN5:
    input:
        meth="methylation/{sample}.RData",
        trainingset_meth=config["trainingset_dir"] + "/{trainingset}.h5"
    output:
        pdf="plots/{sample}-UMAP_kNN-{trainingset}.pdf",
        votes="classification/{sample}-votes-UMAP_kNN-{trainingset}.RData"
    benchmark: "benchmarks/{sample}.UMAP_kNN.{trainingset}.benchmark.txt"
    conda: "envs/tSNE_kNN.yaml"
    threads: 12
    script: "scripts/UMAP_kNN.R"

rule tsne_rf:
    input:
        meth="methylation/{sample}.RData",
        trainingset_meth=config["trainingset_dir"] + "/{trainingset}.h5"
    output:
        pdf="plots/{sample}-tsneRF-{trainingset}.pdf",
        umap="classification/{sample}-tsne-{trainingset}.RData",
        votes="classification/{sample}-votes-tsneRF-{trainingset}.RData"
    benchmark: "benchmarks/{sample}.RF.pancan.{trainingset}.benchmark.txt"
    conda: "envs/tsneRF.yaml"
    threads: 4
    script: "scripts/tsneRF.R"

rule tsne_kNN5:
    input:
        meth="methylation/{sample}.RData",
        trainingset_meth=config["trainingset_dir"] + "/{trainingset}.h5"
    output:
        pdf="plots/{sample}-tsne_kNN-{trainingset}.pdf",
        votes="classification/{sample}-votes-tSNE_kNN-{trainingset}.RData"
    benchmark: "benchmarks/{sample}.tSNE_kNN.pancan.{trainingset}.benchmark.txt"
    conda: "envs/tSNE_kNN.yaml"
    threads: 4
    script: "scripts/tSNE_kNN.R"

rule RF_stratified:
    input:
        meth="methylation/{sample}.RData",
        trainingset_meth=config["trainingset_dir"] + "/{trainingset}.h5"
    params:
        max_CpG = 50000
    output:
        pdf="plots/{sample}-RF_stratified-{trainingset}.pdf",
        matrix=temp("classification/{sample}-matrix-RF_stratified-{trainingset}.RData"),
        votes="classification/{sample}-votes-RF_stratified-{trainingset}.RData"
    benchmark: "benchmarks/{sample}.RF_stratified.{trainingset}.benchmark.txt"
    conda: "envs/tsneRF.yaml"
    threads: 12
    script: "scripts/RF_stratified.R"

rule RF_recal:
    input:
        meth="methylation/{sample}.RData",
        trainingset_meth=config["trainingset_dir"] + "/{trainingset}.h5"
    params:
        max_CpG = 50000
    output:
        pdf="plots/{sample}-RF_recal-{trainingset}.pdf",
        calibration="classification/{sample}-votes-RF_recal-{trainingset}.txt",
        votes="classification/{sample}-votes-RF_recal-{trainingset}.RData"
    benchmark: "benchmarks/{sample}.RF_recal.{trainingset}.benchmark.txt"
    conda: "envs/tsneRF.yaml"
    threads: 12
    script: "scripts/RF_recal.R"



rule RF_stratified_recal:
    input:
        meth="methylation/{sample}.RData",
        trainingset_meth=config["trainingset_dir"] + "/{trainingset}.h5"
    params:
        max_CpG = 50000
    output:
        pdf="plots/{sample}-RF_stratified_recal-{trainingset}.pdf",
        calibration="classification/{sample}-votes-RF_stratified_recal-{trainingset}.txt",
        votes="classification/{sample}-votes-RF_stratified_recal-{trainingset}.RData"
    wildcard_constraints: trainingset = "Capper_et_al"
    benchmark: "benchmarks/{sample}.RF_stratified_recal.{trainingset}.benchmark.txt"
    conda: "envs/tsneRF.yaml"
    threads: 12
    script: "scripts/RF_stratified_recal.R"

rule transform_Rdata:
    input:
        meth="methylation/{sample}.RData",
    output:
        meth_transformed="transfromed_rdata/{sample}-transformed.RData",
    
    conda:"envs/transform_Rdata.yaml"
    threads: 1
    script:
        "scripts/transform_Rdata.R"

rule RF5xCVrecal:
    input:
        meth="transfromed_rdata/{sample}-transformed.RData",
        trainingset_meth=config["trainingset_dir"] + "/{trainingset}.h5"
    params:
        max_CpG = 50000
    output:
        pdf="plots/{sample}-RF_stratified_recal-{trainingset}.pdf",
        calibration="classification/{sample}-votes-RF_stratified_recal-{trainingset}.txt",
        votes="classification/{sample}-votes_tmp-ScikitlearnRF-{trainingset}.RData",
        model_info="classification/{sample}-model_info-ScikitlearnRF-{trainingset}.RData"

    wildcard_constraints: trainingset = "Capper_et_al"
    benchmark: "benchmarks/{sample}.RF_stratified_recal.{trainingset}.benchmark.txt"
    conda: "envs/pyClassifier.yaml"
    threads: 12
    script: "scripts/pyRF5xCVrecal.py"

rule merge_Rdata:
    input:
        votes="classification/{sample}-votes_tmp-ScikitlearnRF-{trainingset}.RData",
        model_info="classification/{sample}-model_info-ScikitlearnRF-{trainingset}.RData"
    output:
        merged_votes="classification/{sample}-votes-ScikitlearnRF-{trainingset}.RData"
    
    conda:"envs/transform_Rdata.yaml"
    threads: 1
    script:
        "scripts/merge_Rdata.R"


rule convert_mc_to_mcf:
    input: 
         data = "classification/{sample}-votes-{method}-Capper_et_al.RData",
         table = "static/MC_MCF.txt"
    output: "classification/{sample}-votes-{method}-Capper_MCF.RData"
    conda: "envs/basicR.yaml"
    script: "scripts/convert_MC_to_MCF.R"

rule summaryClassification_discovery:
    input: expand("classification/{sample}-votes-{{method}}-{{trainingset}}.RData", sample=config["discovery"])
    output: "summaryClassification-{method}-{trainingset}.tsv"
    wildcard_constraints: sample = '|'.join(config["discovery"])
    conda: "envs/basicR.yaml"
    script: "scripts/summaryClassification.R"

rule AUC_ACC_discovery:
    input: expand("classification/{sample}-votes-{{method}}-Capper_MCF.RData", sample=config["discovery"])
    output: "AUC-{method}-Capper_MCF.tsv"
    conda: "envs/auc.yaml"
    script: "scripts/AUC_Hand_Till.R"

rule summaryClassification_validation:
    input: expand("classification/{sample}-votes-{{method}}-{{trainingset}}.RData", sample=config["validation"])
    output: "summaryClassification-validation-{method}-{trainingset}.tsv"
    wildcard_constraints: sample = '|'.join(config["validation"])
    conda: "envs/basicR.yaml"
    script: "scripts/summaryClassification.R"

rule summaryClassification_crosslab:
    input: expand("classification/{sample}-votes-{{method}}-{{trainingset}}.RData", sample=config["PARIS-CROSSLAB"] + config["BERLIN-CROSSLAB"] + config["BASEL-CROSSLAB"] + config["BERLIN-CAPPER-CROSSLAB"])
    output: "summaryCrosslab-{method}-{trainingset}.tsv"
    conda: "envs/basicR.yaml"
    script: "scripts/summaryCrosslab.R"
