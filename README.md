# Robust methylation-based classification of brain tumours using nanopore sequencing

This repository holds source code to reproduce the analysis and results reported in "Robust methylation-based classification of brain tumours using nanopore sequencing"" publication by Kuschel et al. 
It relies on nanoDx analysis pipeline (as a Snakemake subworkflow, which should be located next to the nanoBenchmark analysis pipeline to function properly).

Link to nanoDx pipeline: https://gitlab.com/pesk/nanoDx/

# Reference
If you use the nanoDx pipeline in your research, please consider citing our paper:

Luis P. Kuschel, Jürgen Hench, Stephan Frank, Ivana Bratic Hench, Elodie Girard, Maud Blanluet, Julien Masliah-Planchon, Martin Misch, Julia Onken, Marcus Czabanka, Dongsheng Yuan, Sören Lukassen, Philipp Karau, Naveed Ishaque, Elisabeth G. Hain, Frank Heppner, Ahmed Idbaih, Nikolaus Behr, Christoph Harms, David Capper, Philipp Euskirchen. 2022. "Robust methylation-based classification of brain tumors using nanopore sequencing". Neuropathology and Applied Neurobiology. doi: 10.1111/nan.128567

Link to publication: https://doi.org/10.1111/nan.128567
