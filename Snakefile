configfile: "RXXX_2021.yaml"

### General wildcard constraints:
wildcard_constraints:
    timeframe="\\d+",
    cpg="\\d+",
    seed="\\d+"

### General series definitions

RAW = config["BERLIN-CROSSLAB"] + config["BASEL-CROSSLAB"] + config["BERLIN-CAPPER-CROSSLAB"] + config["samples"]
IMD = config["PARIS-CROSSLAB"]
### Generate methylation calls for local samples using nanoDx workflow

subworkflow nanoDx:
    workdir:
        "../nanoDx"
    configfile:
        "../nanoDx/config.yaml"

rule mirrorMethylationCalls:
    input: nanoDx("methylation/{sample}.RData")
    output: "methylation/{sample}.RData"
    wildcard_constraints: sample = '|'.join(RAW)
    priority: 1000
    shell: "cp {input} {output}"

rule mirrorMethylationCalls_crosslab:
    input: "../nanoDx/methylation/{sample}.RData"
    output: "methylation/{sample}.RData"
    wildcard_constraints: sample = '|'.join(IMD)
    priority: 1000
    shell: "cp {input} {output}"

rule mirrorMethylationCalls_validation:
    input: config["BASEL_basedir"] + "/{sample}.RData"
    output: "methylation/{sample}.RData"
    wildcard_constraints: sample = '|'.join(config["samples-basel"])
    priority: 1000
    shell: "cp {input} {output}"

include: "ML.snakefile"
include: "downsampling.snakefile"
include: "timepoints.snakefile"

rule all_figures:
    input:
       "plots/Table1.html",
       "plots/crosslab_MAF_heatmap.pdf",
       "plots/crosslab_RF.pdf",
       "plots/sankey_mcf_only.html",
       "plots/sankey_mc_mcf.html",
       "plots/Fig3A.pdf",
       "plots/roc_curves.pdf",
       "plots/waterfall_plot_Capper_MCF.pdf",
       "plots/waterfall_plot_Capper_et_al.pdf",
       "plots/supplementary_Figure3.pdf",
       "plots/supplementary_Figure4.pdf"

localrules: merge_subsamples_per_sample, merge_subsamples, downsamplePlot

rule extract_from_RData_to_tsv:
    input: "classification/{sample}-votes-{method}-{trainingset}.RData"
    conda: "envs/basicR.yaml"
    output: "classification/{sample}-votes-{method}-{trainingset}.tsv"
    script: "scripts/extract_from_RData_to_tsv.R"

####

rule perClass_OOB_error:
    input:
      votes = expand("classification/UGS-636-{cpg}-{seed}-votes-{{trainingset}}.RData", seed=1, cpg=config["CpGs"]),
      colorMap = "../nanoDx/static/colorMap_Capper_et_al.txt"
    output: "plots/perClassError_{trainingset}.pdf"
    conda: "envs/basicR.yaml"
    script: "scripts/plot_perClassError.R"

### Randomize discovery and validation cohort 50/50

rule randomize_cohorts:
    input: "static/Ground_Truth_complete.txt"
    output: 
#      gt2019 = "static/Ground_Truth_2019.txt",
      gt2021 = "static/Ground_Truth.txt",
    conda: "envs/basicR.yaml"
    script: "scripts/randomize_cohorts.R"

### Entity overview table

rule make_Table1:
    input:
        ground = "static/Ground_Truth.txt",
	groups = "static/CNS_WHO_2016_groups.txt"
    output: "plots/Table1.html"
    conda: "envs/PDFreport.yaml"
    script: "scripts/make_Table1.R"


### cross-lab validation

rule make_crosslab_heatmap:
    input:
        expand("pileups/paris/{sample}.pileup.txt", sample = config["PARIS-CROSSLAB"]),
        nanoDx(expand("methylation/{sample}.pileup.txt", sample = config["BERLIN-CROSSLAB"] + config["BASEL-CROSSLAB"] + config["BERLIN-CAPPER-CROSSLAB"]))
    output: "plots/crosslab_MAF_heatmap.pdf"
    conda: "envs/basicR.yaml"
    script: "scripts/crosslab_heatmap.R"

rule crosslab_stacked_barplots:
    input:
        paris = expand("classification/{sample}-votes-ScikitlearnRF-Capper_et_al.RData", sample = config["PARIS-CROSSLAB"]),
        berlin = expand("classification/{sample}-votes-ScikitlearnRF-Capper_et_al.RData", sample = config["BERLIN-CROSSLAB"]),
        berlin_capper = expand("classification/{sample}-votes-ScikitlearnRF-Capper_et_al.RData", sample = config["BERLIN-CAPPER-CROSSLAB"]),
        basel = expand("classification/{sample}-votes-ScikitlearnRF-Capper_et_al.RData", sample = config["BASEL-CROSSLAB"]),
        colormap = "static/colorMap_Capper_et_al.txt"
    output: "plots/crosslab_RF.pdf"
    conda: "envs/basicR.yaml"
    script: "scripts/crosslab_stacked_barplots.R"

#### Figure 1 - Downsampling Plot + Sankey plot validation series

rule groundTruthPlot:
    input:
      ground_truth="static/Ground_Truth.txt",
      summary_classification="summaryClassification-validation-ScikitlearnRF-Capper_MCF.tsv",
      summary_classification_mc="summaryClassification-validation-ScikitlearnRF-Capper_et_al.tsv"
    output:
      "plots/sankey_mcf_only.html",
      "plots/sankey_mc_mcf.html"
    conda: "envs/sankey.yaml"
    script: "scripts/groundTruthPlot.R"

#### ROC curves

rule roc_curve:
    input:
        subclass = "summaryClassification-ScikitlearnRF-Capper_et_al.tsv",
        mcf = "summaryClassification-ScikitlearnRF-Capper_MCF.tsv"
    output:
        ROC = "plots/roc_curves.pdf",
        ROC_MC = "plots/ROC_Capper_et_al.pdf",
        ROC_MCF = "plots/ROC_Capper_MCF.pdf",
    conda: "envs/basicR.yaml"
    script: "scripts/rocit.R"

#### Waterfall plot

rule waterfall_plot:
    input:
        discovery = "summaryClassification-ScikitlearnRF-{trainingset}.tsv",
        validation = "summaryClassification-validation-ScikitlearnRF-{trainingset}.tsv",
        gt = "static/Ground_Truth.txt"
    output: "plots/waterfall_plot_{trainingset}.pdf"
    conda: "envs/basicR.yaml"
    script: "scripts/waterfall_plot.R"

#### nanoBenchmark analysis

rule nanoBenchmark_analysis:
    input:
        sum_seq_time = "summary_sequencing_time_filtered.tsv",
        rc = "redcap_extracted_data.tsv",
        pre_handson = "pre_handson_extracted_data.tsv",
        ground_truth = "static/Ground_Truth.txt",
        downsampling = "summaryDownsampling_Capper_et_al.tsv",
        classification = "summaryClassification_Capper_et_al.tsv"
    params: config["PROSPECTIVE"]
    output: "nanoBenchmark_Analysis.RData"
    conda: "envs/basicR.yaml"
    script: "scripts/nanoBenchmark_Analysis.R"


#### Plot standard vs. recalibrated nanoDx classifier results

rule standard_vs_recalibrated_nanoDx:
    input:
        standard = "summaryClassification_Capper_et_al.tsv",
        recalibrated = "summaryClassification_recalibrated_Capper_et_al.tsv"
    output: "plots/standard_vs_recalibrated_nanoDx.pdf"
    conda: "envs/basicR.yaml"
    script: "scripts/standard_vs_recalibrated_nanoDx.R"

#### Supplementary Table 1

rule make_supplementary_table1:
    input:
        ground = "static/Ground_Truth.txt",
        sum_disc_mcf = "summaryClassification-ScikitlearnRF-Capper_MCF.tsv",
        sum_disc_mc = "summaryClassification-ScikitlearnRF-Capper_et_al.tsv",
        sum_vali_mcf = "summaryClassification-validation-ScikitlearnRF-Capper_MCF.tsv",
        sum_vali_mc = "summaryClassification-validation-ScikitlearnRF-Capper_et_al.tsv",
        nanostat = nanoDx(expand("stats/{sample}.nanostat.tsv", sample=config["samples"])),
        mosdepth = nanoDx(expand("stats/{sample}.mosdepth.summary.txt", sample=config["samples"]))
    output: "Supplementary_Table1.tsv"
    conda: "envs/basicR.yaml"
    script: "scripts/supplementary_table1.R"

#### Supplementary Table 2

rule make_supplementary_table2:
    input:
        mc = "summaryCrosslab-ScikitlearnRF-Capper_et_al.tsv",
        mcf = "summaryCrosslab-ScikitlearnRF-Capper_MCF.tsv"
    output: "Supplementary_Table2.tsv"
    conda: "envs/basicR.yaml"
    script: "scripts/supplementary_table2.R"


#### Supplementary Figure 3

rule make_supplementary_Figure3:
    input:
        gt = "static/Ground_Truth.txt",
        ds = "summaryDownsampling_Capper_MCF.tsv",
        sc = "summaryClassification-ScikitlearnRF-Capper_MCF.tsv",
        sc2 = "summaryClassification-validation-ScikitlearnRF-Capper_MCF.tsv"
    output:
    	standard = "plots/supplementary_Figure3.pdf",
    	error_bars = "plots/supplementary_Figure3_error_bars.pdf"
    conda: "envs/basicR.yaml"
    script: "scripts/Suppl_Fig3.R"

#### Supplementary Figure 4

rule make_supplementary_Figure4:
    input:
        gt = "static/Ground_Truth.txt",
        tp = "summary_sequencing_time_Capper_MCF.tsv",
    output: "plots/supplementary_Figure4.pdf"
    conda: "envs/basicR.yaml"
    script: "scripts/Suppl_Fig4.R"

#### Barcode statistics
rule make_demux_stat_tsv_per_sample:
    input: nanoDx("fastq/{sample}.fq")
    output: temporary("tables/{sample}_demux_stat.tsv")
    conda: nanoDx("envs/demux.yaml")
    shell: """qcat -f {input} --tsv > {output}"""

rule count_barcodes_per_sample:
    input: "tables/{sample}_demux_stat.tsv"
    output: "tables/{sample}_barcode_counts.tsv"
    conda: "envs/basicR.yaml"
    script: "scripts/count_barcodes.R"

rule merge_barcode_counts:
    input: expand("tables/{sample}_barcode_counts.tsv", sample = config["samples"])
    output: "summaryBarcoding.tsv"
    shell: "head -1 {input[0]} > {output} ; tail -n +2 -q {input} >> {output}"

#### HTML statistics

rule make_html_summary:
   input: 
       DS = "summaryDownsampling_Capper_MCF.tsv",
       DISC_MC = "summaryClassification-ScikitlearnRF_recal-Capper_et_al.tsv",
       DISC_MCF = "summaryClassification-ScikitlearnRF-Capper_MCF.tsv",
       VALI_MC = "summaryClassification-validation-ScikitlearnRF-Capper_et_al.tsv",
       VALI_MCF = "summaryClassification-validation-ScikitlearnRF-Capper_MCF.tsv",
       GT = "static/Ground_Truth.txt",
       BAR_COUNT = "summaryBarcoding.tsv"
   output: "Analysis_summary.html"
   conda: "envs/basicR.yaml"
   script: "scripts/make_HTML_summary.R"
